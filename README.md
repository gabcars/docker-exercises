# docker-exercises

Docker for developers tutorial

**Ready to begin?**

Head over to [the first exercise](/01-hello-world.md) to begin.

### Misc

These exercises build upon and have been inspired by the following resources:

* https://github.com/praqma-training/docker-katas
* http://docker.atbaker.me/index.html
* https://github.com/docker/labs/tree/master/beginner

